from SlideReader import SlideReader
from Patch import Patch
from PIL import Image
import numpy as np
import random


slidepath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Train_Tumor/Tumor_005.tif'
annotpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/XML/Tumor_005.xml'
maskpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/Mask/Tumor_005_Mask.tif'

patch_hor_size = 500
patch_ver_size = 500
number_of_patch = 1000
level = 0

reader = SlideReader(slidepath, maskpath)
dims=reader.get_level_slide_dims(level)
slide_ver_size = dims[0]
slide_hor_size  = dims[1]

print "vertical size : " , slide_ver_size
print "horizontal size    : " , slide_hor_size

row_count = slide_ver_size / patch_ver_size
column_count = slide_hor_size / patch_hor_size

total_squares = row_count * column_count
patch_list = np.arange(total_squares)
selected_patchs = random.sample(patch_list, number_of_patch)
#print column_count

for i in range(number_of_patch):
    row_position = selected_patchs[i] / column_count
    column_position = selected_patchs[i] % column_count

    if row_position == 0:
        row_position = 1
    if column_position != 0:
        row_position = row_position + 1

    patch_x_location = row_position * patch_hor_size - patch_hor_size
    patch_y_location = column_position * patch_ver_size - patch_ver_size
    patch = Patch(location=[patch_x_location, patch_y_location], size=[patch_hor_size, patch_ver_size])
    img = Image.fromarray(reader.read_area_content(level, patch))
    img.save("/home/oguzhaner/Desktop/thesis/" + str(selected_patchs[i]) + ".png")

    #print selected_squares[i]
    #print row_position
    #print column_position
    #print patch_x_location
    #print patch_y_location
    #patch = Patch(location=[50000, 123250], size=[patch_hor_size, patch_ver_size])
#print row_count
#print column_count


#print square_list.size

#img.save("/home/oguzhaner/Desktop/thesis/" + str(i) +".png")



#patch = Patch(location=[50000,123250],size=[patch_hor_size,patch_ver_size])
#patch2 = Patch(location=[0,0],size=[size_of_area,size_of_area])
#img = Image.fromarray(reader.read_area_content(0,patch))
#img2 = Image.fromarray(reader.read_area_content(0,patch2))
#img.show();
#img2.show();

