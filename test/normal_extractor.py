from SlideReader import SlideReader
from Patch import Patch
from PIL import Image
import numpy as np
from skimage.color import rgb2gray
import skimage
from skimage import data, io
from skimage.viewer import ImageViewer
import datetime
np.set_printoptions(threshold=np.nan)

##Constants##
initial_slide_level = 5
slide_level_zero = 0
sample_patch_size = 500
valid_bw_value = 0.7
sample_count = 3000
maskpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/Mask/Tumor_005_Mask.tif'

filename = "/home/oguzhaner/Desktop/analysisOfPatch"
file = open(filename, 'w')
total_regions = 0

def calculate_sample_per_region(area):
    print area
    area = np.asarray(area)
    area = np.divide(area,100000)
    print area
    total_weight = sum(area)
    print total_weight
    region_sample_ratio = float(sample_count) / float(total_weight)
    print region_sample_ratio
    area = area * region_sample_ratio
    return area

def create_patch(Ibw,slideNo):
    Ilbl = skimage.morphology.label(Ibw)
    num_regions = np.max(Ilbl)
    global total_regions
    total_regions += num_regions
    total_patch_count = 0
    size_of_regions=[];
    slideName = "slide:" + str(slideNo) + " total region:" + str(num_regions) + "\n"
    file.write(slideName)
    if num_regions == 0:
        return

    # for rr in range(num_regions):
    #     Irr = Ilbl == (rr + 1)
    #     active_area = np.where(Irr)
    #
    #     xmin = np.max(np.min(active_area[1]) - 15, 0) * (2 ** initial_slide_level);
    #     xmax = np.max(active_area[1]) * (2 ** initial_slide_level)
    #     ymin = np.max(np.min(active_area[0]) - 15, 0) * (2 ** initial_slide_level);
    #     ymax = np.max(active_area[0]) * (2 ** initial_slide_level)
    #
    #     xsize = xmax - xmin
    #     ysize = ymax - ymin
    #     area = xsize * ysize
    #     size_of_regions.append(area)

    # sample_count_per_region = calculate_sample_per_region(size_of_regions)

    #
    # for rr in range(num_regions):
    #
    #     patch_number = 0
    #     total_try = 0
    #
    #     Irr = Ilbl == (rr + 1)
    #     active_area = np.where(Irr)
    #
    #     xmin = np.max(np.min(active_area[1]) - 15, 0) * (2 ** initial_slide_level);
    #     xmax = np.max(active_area[1]) * (2 ** initial_slide_level)
    #     ymin = np.max(np.min(active_area[0]) - 15, 0) * (2 ** initial_slide_level);
    #     ymax = np.max(active_area[0]) * (2 ** initial_slide_level)
    #
    #     xsize = xmax - xmin
    #     ysize = ymax - ymin
    #     area = xsize * ysize
    #
    #     file.write("region" + str(rr) + ":" + str(area) + "\n")
    #
    #
    #     while (patch_number < sample_count):
    #         xrandpos = np.floor(np.random.random() * xsize) + xmin
    #         yrandpos = np.floor(np.random.random() * ysize) + ymin
    #         patch = Patch(location=np.asarray([int(xrandpos), int(yrandpos)]),
    #                       size=[sample_patch_size, sample_patch_size])
    #         Ipatch = reader.read_area_content(slide_level_zero, patch)
    #         Ig_patch = skimage.color.rgb2gray(Ipatch)
    #         Ibw_patch = (Ig_patch > 0.04) * (Ig_patch < 0.7)
    #         total_try += 1
    #         if total_try > (sample_count * 4):
    #             print "break"
    #             break
    #         if (np.mean(Ibw_patch) > valid_bw_value):
    #             OriginalImage = Image.fromarray(Ipatch)
    #             OriginalImage.save("/run/media/oguzhaner/Seagate Expansion Drive/Thesis/data_set/normal/normal" + str(number) + "_patch_" + str(patch_number) + "_cca_" + str(rr) + ".jpg")
    #             patch_number += 1
    #             # print patch_number
    #     total_patch_count+=patch_number
    #     file.write("region" + str(rr) + ":" + str(patch_number) +"\n")
    # if(total_patch_count < sample_count):
    #     missing_slide = sample_count - total_patch_count
    #     file.write("missing slide: " + str(missing_slide) + "\n")



for number in range(1,111):
    start_time = datetime.datetime.now()
    normalpath = '/run/media/oguzhaner/Seagate Expansion Drive/Thesis/camelyon16/TrainingData/Train_Normal/Normal_'
    if number== 108 or number== 27 or number==45 or number==16 or number==13 \
            or number == 38 or number == 59 or number == 42 or number == 29:
        continue
    if number < 10:
        normalpath = normalpath + "00" + str(number) + ".tif"
    if (number >= 10) and (number <100):
        normalpath = normalpath + "0" + str(number) + ".tif"
    if  (number >= 100):
       normalpath = normalpath + str(number) + ".tif"

    print normalpath
    reader = SlideReader(normalpath, maskpath)
    dims = reader.get_level_slide_dims(initial_slide_level)
    patch = Patch(location=[0, 0], size=[dims[0], dims[1]])
    print dims
    I = reader.read_area_content(initial_slide_level, patch)
    Ig = skimage.color.rgb2gray(I)
    Ibw = (Ig > 0.035) * (Ig < 0.65)
    Ibw = skimage.morphology.remove_small_holes(Ibw, 20000)
    Ibw = skimage.morphology.remove_small_objects(Ibw, 20000)

    Ibw[0:700, :] = False;
    Ibw[dims[1]-210:dims[1], :] = False;
    Ibw[:,0:100] = False;
    Ibw = skimage.morphology.remove_small_objects(Ibw, 10000)

    # ImageIBW = Ibw.astype('uint8') * 255
    # OriginalImage = Image.fromarray(ImageIBW)
    # OriginalImage.save("/home/oguzhaner/Desktop/images2BW/slide" + str(number) + ".jpg")
    # OriginalImage.show()
    create_patch(Ibw,number)

print "total regions : " + str(total_regions)
end_time = datetime.datetime.now()
file.write("\nDuration : " + str(end_time - start_time))
file.close()