from SlideReader import SlideReader
from Patch import Patch
from PIL import Image
import numpy as np
from skimage.color import rgb2gray
import skimage
from skimage import data, io
from skimage.viewer import ImageViewer

##Constants##
initial_slide_level = 6
slide_level_zero = 0
sample_patch_size = 500
valid_bw_value = 0.75
sample_count = 1000

slidepath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Train_Tumor/Tumor_005.tif'
annotpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/XML/Tumor_005.xml'
maskpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/Mask/Tumor_005_Mask.tif'

reader = SlideReader(slidepath, maskpath)
dims = reader.get_level_slide_dims(initial_slide_level)

patch = Patch(location=[0, 0], size=[dims[0], dims[1]])
I = reader.read_area_content(initial_slide_level, patch)
Ig = skimage.color.rgb2gray(I)
Ibw = (Ig > 0.04) * (Ig < 0.7)
Ibw = skimage.morphology.remove_small_holes(Ibw, 20000)
Ibw = skimage.morphology.remove_small_objects(Ibw, 20000)
Ibw[0:2000, :] = False;
Ibw[12000:14000, :] = False;

ImageIBW = Ibw.astype('uint8')*255
im = Image.fromarray(ImageIBW)
# im.show()

Ilbl = skimage.morphology.label(Ibw)
num_regions = np.max(Ilbl)

# sz=reader.mask.level_dimensions[6]
#
# Imask = np.array(reader.mask.read_region((0,0), 6, (sz[0],sz[1])))[:,:,0]*1.0
# ImaskShow = Image.fromarray(Imask)
#

for rr in range(num_regions):
    cnt=0
    Irr = Ilbl==(rr+1)
    active_area=np.where(Irr)

    xmin=np.max(np.min(active_area[1])-15,0)*64; xmax=np.max(active_area[1])*64
    ymin=np.max(np.min(active_area[0])-15,0)*64; ymax=np.max(active_area[0])*64
    xsize=xmax-xmin
    ysize=ymax-ymin

    print xmin
    print xmax
    print ymax
    print ymin

    if(xsize < 2000 or ysize < 2000):
        continue

    counter = 0
    while (counter < sample_count):
        xrandpos = np.floor(np.random.random() * xsize) + xmin
        yrandpos = np.floor(np.random.random() * ysize) + ymin
        patch = Patch(location=np.asarray([int(xrandpos),int(yrandpos)]),size=[sample_patch_size,sample_patch_size])
        Ipatch = reader.read_area_content(slide_level_zero, patch)
        Ig_patch = skimage.color.rgb2gray(Ipatch)
        Ibw_patch = (Ig_patch > 0.04) * (Ig_patch < 0.7)
        if np.mean(Ibw_patch) > valid_bw_value:
            label = reader.get_area_label(patch)
            OriginalImage = Image.fromarray(Ipatch)
            maskImage = Image.fromarray(reader.get_mask_content(patch,0))

            if label < 0:
                OriginalImage.save("/home/oguzhaner/Desktop/thesis/img" + " " + str(rr) + str(counter) + ".jpg")
                maskImage.save("/home/oguzhaner/Desktop/thesis/img" + " " + str(rr) + str(counter) + ".jpg")
            else:
                OriginalImage.save("/home/oguzhaner/Desktop/thesis/patch/img" + " " + str(rr) + str(counter) + ".jpg")
                maskImage.save("/home/oguzhaner/Desktop/thesis/patch/img" + " " + str(rr) + str(counter) + ".jpg")
            counter +=1

