from SlideReader import SlideReader
from Patch import Patch
from PIL import Image
import numpy as np
from skimage.color import rgb2gray
import skimage
from skimage import data, io
from skimage.viewer import ImageViewer
from skimage.measure import label, regionprops

np.set_printoptions(threshold=np.nan)

##Constants##
initial_slide_level = 5
slide_level_zero = 0
sample_patch_size = 500
valid_bw_value = 0.7
sample_count = 3000
maskpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/Mask/Tumor_005_Mask.tif'

filename = "/home/oguzhaner/Desktop/analysisOfPatch"
file = open(filename, 'w')

def create_patch(Ibw,slideNo,reader):
    Ilbl = skimage.morphology.label(Ibw)
    num_regions = np.max(Ilbl)
    slideName = "slide:" + str(slideNo) + " total region:" + str(num_regions) + "\n"
    file.write(slideName)
    total_patch_count = 0
    order_of_regions=[];
    for rr in range(num_regions):
        Irr = Ilbl == (rr + 1)
        active_area = np.where(Irr)

        xmin = np.max(np.min(active_area[1]) - 15, 0) * (2 ** initial_slide_level);
        xmax = np.max(active_area[1]) * (2 ** initial_slide_level)
        ymin = np.max(np.min(active_area[0]) - 15, 0) * (2 ** initial_slide_level);
        ymax = np.max(active_area[0]) * (2 ** initial_slide_level)


        patch_number = 0
        total_try = 0
        # print ymin
        # print xmin
        # print ymax
        # print xmax
        ysize = ymax - ymin
        xsize = xmax - xmin
        area = xsize * ysize

        file.write("region" + str(rr) + ":" + str(area) + "\n")

    #     while (patch_number < (sample_count / num_regions)):
    #
    #         xrandpos = np.floor(np.random.random() * xsize) + xmin
    #         yrandpos = np.floor(np.random.random() * ysize) + ymin
    #         patch = Patch(location=np.asarray([int(xrandpos), int(yrandpos)]),
    #                       size=[sample_patch_size, sample_patch_size])
    #         Ipatch = reader.read_area_content(slide_level_zero, patch)
    #         Ig_patch = skimage.color.rgb2gray(Ipatch)
    #         Ibw_patch = (Ig_patch > 0.04) * (Ig_patch < 0.7)
    #         total_try += 1
    #         if total_try > (sample_count * 2):
    #             print "break"
    #             break
    #         if (np.mean(Ibw_patch) > valid_bw_value):
    #             # OriginalImage = Image.fromarray(Ipatch)
    #             # OriginalImage.save("/run/media/oguzhaner/Seagate Expansion Drive/Thesis/data_set/normal/normal" + str(number) + "_patch_" + str(patch_number) + "_cca_" + str(rr) + ".jpg")
    #             # OriginalImage.save("/home/oguzhaner/Desktop/slide1/" + str(patch_number) + "_cca_" + str(rr) + ".jpg")
    #             patch_number += 1
    #             # print patch_number
    #     total_patch_count+=patch_number
    #     file.write("region" + str(rr) + ":" + str(patch_number) +"\n")
    # if(total_patch_count < sample_count):
    #     missing_slide = sample_count - total_patch_count

for number in range(108,109):


    normalpath = '/run/media/oguzhaner/Seagate Expansion Drive/Thesis/camelyon16/TrainingData/Train_Normal/Normal_'
    if number < 10:
        normalpath = normalpath + "00" + str(number) + ".tif"
    if (number >= 10) and (number <100):
        normalpath = normalpath + "0" + str(number) + ".tif"
    if  (number >= 100):
       normalpath = normalpath + str(number) + ".tif"

    print normalpath
    # normalpath = "/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Train_Tumor/Tumor_005.tif"
    reader = SlideReader(normalpath, maskpath)
    dims = reader.get_level_slide_dims(initial_slide_level)
    patch = Patch(location=[0, 0], size=[dims[0], dims[1]])
    I = reader.read_area_content(initial_slide_level, patch)
    Ig = skimage.color.rgb2gray(I)
    Ibw = (Ig > 0.04) * (Ig < 0.7)
    Ibw = skimage.morphology.remove_small_holes(Ibw, 20000)
    Ibw = skimage.morphology.remove_small_objects(Ibw, 20000)
    Ibw[0:710, :] = False;
    Ibw[:,0:300] = False;
    Ibw[12000:14000, :] = False;
    ImageIBW = Ibw.astype('uint8') * 255
    OriginalImage = Image.fromarray(ImageIBW)
    OriginalImage.show()
    # create_patch(Ibw,number,reader)

file.close()

