from SlideReader import SlideReader
from Patch import Patch
from PIL import Image
import numpy as np
from skimage.color import rgb2gray
import skimage
from skimage import data, io
from skimage.viewer import ImageViewer
from skimage.measure import label, regionprops

##Constants##
initial_slide_level = 6
mask_level_zero = 0
sample_patch_size = 3000
valid_bw_value = 0.75
sample_count = 1000
initial_mask_level = 6

slidepath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Train_Tumor/Tumor_005.tif'
annotpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/XML/Tumor_005.xml'

total_mask=0
total_region=0
total_out_size=0

for rr in range(1,111):
    maskpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/Mask/Tumor_'
    if rr < 10:
        maskpath = maskpath + "00" + str(rr) + "_Mask.tif"
    if (rr >= 10) and (rr <100):
        maskpath = maskpath + "0" + str(rr) + "_Mask.tif"
    if  (rr >= 100):
        maskpath = maskpath + str(rr) + "_Mask.tif"

    reader = SlideReader(slidepath, maskpath)
    dims = reader.get_level_mask_dims(initial_mask_level)

    patch = Patch(location=[0, 0], size=[dims[0], dims[1]])
    I = reader.get_mask_content(patch,initial_mask_level)
    maskShow=Image.fromarray(I)
    #maskShow.show()
    Ilbl = skimage.morphology.label(I)

    regions = regionprops(Ilbl.astype(int))
    counter = 0
    print rr
    #print maskpath
    for prop in regions:
        #print "region" + str(counter)
        total_region=total_region+1
        counter=counter+1
        y0,x0 = prop.centroid
        level_zero_x = x0*64
        level_zero_y = y0*64
        xmin = level_zero_x -(sample_patch_size/2); xmax = level_zero_x +(sample_patch_size/2);
        ymin = level_zero_y -(sample_patch_size/2); ymax = level_zero_y+(sample_patch_size/2);
        if(xmin < 0):
            xmin=0
        if(ymin < 0):
            ymin=0
        xsize=xmax-xmin
        ysize=ymax-ymin

        #Bounding Box
        min_row,min_col,max_row,max_col = prop.bbox
        min_row= min_row*64
        max_row=max_row*64
        min_col=min_col*64
        max_col=max_col*64

        # print "ymin : " + str(ymin)
        # print "min_row : " + str(min_row)
        # print "ymax : " + str(ymax)
        # print "max_row : " + str(max_row)
        #
        # print "xmin : " + str(xmin)
        # print "min_col : " + str(min_col)
        # print "xmax : " + str(xmax)
        # print "max_col : " +str(max_col)

        over_left = ((xmin - min_col) > 0)
        over_right = ((xmax -max_col) < 0)
        over_up= ((ymin - min_row) > 0)
        over_down = ((ymax - max_row) < 0)

        if(over_left):
            total_out_size=total_out_size+1
            # print "over left"
        elif (over_right):
            total_out_size = total_out_size + 1
            # print "over right"
        elif(over_down):
            total_out_size = total_out_size + 1
            # print "over down"
        elif(over_up):
            total_out_size = total_out_size + 1
            # print "over up"
        # patch = Patch(location=np.asarray([int(xmin), int(ymin)]), size=[sample_patch_size, sample_patch_size])
        # Ipatch = reader.get_mask_content(patch, mask_level_zero)
        # OriginalImage = Image.fromarray(Ipatch)
        # OriginalImage.convert('RGB').save("/home/oguzhaner/Desktop/thesis/mask/" + "mask" + str(rr) + "region" + str(counter)  + ".jpg")
print total_out_size
print total_region