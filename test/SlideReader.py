import openslide
import numpy as np
import xml.etree.ElementTree as ET
from shapely.geometry import Polygon

class SlideReader:
    
    def __init__(self,slidepath,maskpath,annotpath=None):
        
        self.slide=openslide.open_slide(slidepath)
        self.mask=openslide.open_slide(maskpath)
                    
    def get_level_slide_dims(self,level):
        return self.slide.level_dimensions[level]

    def get_level_mask_dims(self, level):
        return self.mask.level_dimensions[level]

    def read_area_content(self,level,patch):
        loc = patch.get_location();   siz = patch.get_size()
        pixelarray = np.array(self.slide.read_region((loc[0],loc[1]), level, (siz[0],siz[1])))
        return pixelarray
        
    def get_area_label(self,patch):
        #loc = patch.get_location().astype(int);   siz = patch.get_size().astype(int)
        pixelarray = self.get_mask_content(patch,0)

        ratio =  np.mean(pixelarray)
        print (ratio)
        if ratio > 0.75:
            print (ratio)
        return (ratio > 0.75)

    def get_level_count(self):
        return self.slide.level_count()

    def get_contain_tumor(self,patch):
        pixelarray = self.get_mask_content(patch, 0)

        ratio = np.mean(pixelarray)
        # if ratio > 0.75:
        #     print (ratio)
        return (ratio > 0.75)

    def get_mask_content(self,patch,level):
        loc = patch.get_location()
        siz = patch.get_size()

        pixelarray = np.array(self.mask.read_region((loc[0], loc[1]), level, (siz[0], siz[1])))[:, :, 0] *1.0
        return pixelarray