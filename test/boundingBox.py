from calendar import IllegalMonthError

from matplotlib.pyplot import yscale

from SlideReader import SlideReader
from Patch import Patch
from PIL import Image
import numpy as np
import random
from skimage.color import rgb2gray
import skimage
from skimage import data, io
from skimage.viewer import ImageViewer
import sys
np.set_printoptions(threshold=sys.maxint)
slidepath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Train_Tumor/Tumor_005.tif'
annotpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/XML/Tumor_005.xml'
maskpath = '/home/oguzhaner/MSC/thesis/camelyon16/TrainingData/Ground_Truth/Mask/Tumor_005_Mask.tif'

reader = SlideReader(slidepath, maskpath)
dims = reader.get_level_slide_dims(6)

patch = Patch(location=[0, 0], size=[dims[0], dims[1]])
I = reader.read_area_content(6, patch)
Ig = skimage.color.rgb2gray(I)
Ibw = (Ig > 0.04) * (Ig < 0.7)
Ibw = skimage.morphology.remove_small_holes(Ibw, 20000)
Ibw = skimage.morphology.remove_small_objects(Ibw, 20000)
Ibw[0:2000, :] = False;  # Ibw[:,0:200] = False
Ibw[12000:14000, :] = False;  # Ibw[:,2900:3000] = False

ImageIBW = Ibw.astype('uint8')*255
im = Image.fromarray(ImageIBW)
#im.show()


#np.savetxt("/home/oguzhaner/Desktop/IBW",Ibw,fmt='%d')

Ilbl = skimage.morphology.label(Ibw)
#print Ilbl
#np.savetxt("/home/oguzhaner/Desktop/ILBL",Ilbl,fmt='%d')

num_regions = np.max(Ilbl)

#print num_regions
sz=reader.mask.level_dimensions[6]
Imask = np.array(reader.mask.read_region((0,0), 6, (sz[0],sz[1])))[:,:,0]*1.0
ImaskShow = Image.fromarray(Imask)
#ImaskShow.show()
patch_size = 256
for rr in range(num_regions):
    cnt=0
    Irr = Ilbl==(rr+1) # calisilan
    #np.savetxt("/home/oguzhaner/Desktop/IRR" + str(rr),Irr,fmt='%d')
    #print Ilbl[rr]
    #print Irr
    active_area=np.where(Irr>0)
    #np.savetxt("/home/oguzhaner/Desktop/ActiveArea0_" + str(rr), active_area[0], fmt='%d')
    #np.savetxt("/home/oguzhaner/Desktop/ActiveArea1_" + str(rr), active_area[1], fmt='%d')
    #print active_area
    xmin=np.max(np.min(active_area[1])-15,0)*64; xmax=np.max(active_area[1])*64
    ymin=np.max(np.min(active_area[0])-15,0)*64; ymax=np.max(active_area[0])*64
    #print np.max(np.min(active_area[1])-15,0)

    xsize=xmax-xmin
    ysize = ymax - ymin

    #print "xrandpos: " + str(int(xrandpos))
    #print "yrandpos: " + str(int(yrandpos))
    #print "xmin : " + str(xmin)
    #print "xmax : " + str(xmax)
    #print "ymin : " + str(ymin)
    #print "ymax : " + str(ymax)
    #print "xsize : " + str(xsize)
    #print "ysize : " + str(ysize)

    counter = 0
    while (counter < 9):
        xrandpos = np.floor(np.random.random() * xsize) + xmin
        yrandpos = np.floor(np.random.random() * ysize) + ymin
        patch = Patch(location=np.asarray([int(xrandpos),int(yrandpos)]),size=[500,500])
        Ipatch = reader.read_area_content(0, patch)
        Ig_patch = skimage.color.rgb2gray(Ipatch)
        Ibw_patch = (Ig_patch > 0.04) * (Ig_patch < 0.7)
        print np.mean(Ibw_patch)
        if np.mean(Ibw_patch) > 0.75:
            OriginalImage = Image.fromarray(Ipatch)
            OriginalImage.save("/home/oguzhaner/Desktop/img" + " " + str(rr) +str(counter)+ ".jpg")
            counter +=1
            print "COUNTER !! !! !! " + str(counter)




#ImageBW = Image.fromarray(Ig)
#ImageBW.show()

#print Ig[0].size

#print Ibw[0]

#Ig = Image.fromarray(I).convert('LA')
#Ig.show()



#OriginalImage = Image.fromarray(I)
#OriginalImage.show()
#OriginalImage.save("/home/oguzhaner/Desktop/img.jpg")

#img = Image.open('/home/oguzhaner/Desktop/img.jpg')
#img.show()
#print img

#Ig = skimage.color.rgb2gray(I)



#GrayScaleImage = Image.fromarray(Ig)
#GrayScaleImage.show()
#img = Image.fromarray(Ig)
