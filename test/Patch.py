import numpy as np

class Patch:
    
    def __init__(self,location,size):
        
        self.location = location # top left point
        self.size = size        
    
    def get_location(self):
        
        return self.location
        
    def get_size(self):        
        
        return self.size
       