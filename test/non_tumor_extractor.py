from SlideReader import SlideReader
from Patch import Patch
from PIL import Image
import numpy as np
from skimage.color import rgb2gray
import skimage
from skimage import data, io
from skimage.viewer import ImageViewer

##Constants##
initial_slide_level = 5
slide_level_zero = 0
sample_patch_size = 500
valid_bw_value = 0.75
sample_count = 3000


for number in range(1,111):
    maskpath = '/run/media/oguzhaner/Seagate Expansion Drive/Thesis/camelyon16/TrainingData/Ground_Truth/Mask/Tumor_'
    slidepath = '/run/media/oguzhaner/Seagate Expansion Drive/Thesis/camelyon16/TrainingData/Train_Tumor/Tumor_'
    print "Slide number : " + str(number)
    if number < 10:
        maskpath = maskpath + "00" + str(number) + "_Mask.tif"
        slidepath = slidepath + "00" + str(number) + ".tif"
    if (number >= 10) and (number <100):
        maskpath = maskpath + "0" + str(number) + "_Mask.tif"
        slidepath = slidepath + "0" + str(number) + ".tif"
    if  (number >= 100):
       maskpath = maskpath + str(number) + "_Mask.tif"
       slidepath = slidepath + str(number) + ".tif"

    reader = SlideReader(slidepath, maskpath)
    dims = reader.get_level_slide_dims(initial_slide_level)
    patch = Patch(location=[0, 0], size=[dims[0], dims[1]])
    I = reader.read_area_content(initial_slide_level, patch)


    # OriginalImage = Image.fromarray(I)
    # OriginalImage.save("/home/oguzhaner/Desktop/slides/slide" + str(number) + ".jpg")

    Ig = skimage.color.rgb2gray(I)
    Ibw = (Ig > 0.04) * (Ig < 0.7)
    Ibw = skimage.morphology.remove_small_holes(Ibw, 20000)
    Ibw = skimage.morphology.remove_small_objects(Ibw, 20000)
    Ibw[dims[1] - 210:dims[1], :] = False;
    Ibw[:, 0:100] = False;
    if number < 71:
        Ibw[0:900, :] = False;
    else:
        Ibw[:, 0:120] = False;
        Ibw[0:200, :] = False;
        if number==102 or number==92 or number==79 or number==77:
            Ibw[:, 0:300] = False;


    Ibw = skimage.morphology.remove_small_objects(Ibw, 10000)
    # ImageIBW = Ibw.astype('uint8') * 255
    # OriginalImage = Image.fromarray(ImageIBW)
    # OriginalImage.save("/home/oguzhaner/Desktop/slidesBW/slide" + str(number) + ".jpg")
    # OriginalImage.show()

    Ilbl = skimage.morphology.label(Ibw)
    num_regions = np.max(Ilbl)

    print "total regions : "  + str(num_regions)
    for rr in range(num_regions):
        patch_number = 0
        Irr = Ilbl==(rr+1)
        active_area=np.where(Irr)
        total_try = 0
        xmin=np.max(np.min(active_area[1])-15,0)*(2**initial_slide_level); xmax=np.max(active_area[1])*(2**initial_slide_level)
        ymin=np.max(np.min(active_area[0])-15,0)*(2**initial_slide_level); ymax=np.max(active_area[0])*(2**initial_slide_level)

        xsize=xmax-xmin
        ysize=ymax-ymin

        if(xsize < 2000 or ysize < 2000):
            print "continue"
            continue

        while (patch_number < sample_count):
            xrandpos = np.floor(np.random.random() * xsize) + xmin
            yrandpos = np.floor(np.random.random() * ysize) + ymin
            patch = Patch(location=np.asarray([int(xrandpos),int(yrandpos)]),size=[sample_patch_size,sample_patch_size])
            Ipatch = reader.read_area_content(slide_level_zero, patch)
            Ig_patch = skimage.color.rgb2gray(Ipatch)
            Ibw_patch = (Ig_patch > 0.04) * (Ig_patch < 0.7)
            total_try += 1

            if total_try > (sample_count * 4):
                print "break"
                break
            if np.mean(Ibw_patch) > valid_bw_value:
                label = reader.get_contain_tumor(patch)
                if(label != 1):
                    OriginalImage = Image.fromarray(Ipatch)
                    OriginalImage.save("/run/media/oguzhaner/Seagate Expansion Drive/Thesis/data_set/tumor/slide_normal/slide_normal_" + str(number) + "_patch_" + str(patch_number) + "_cca_" + str(rr) + ".jpg")
                    patch_number +=1

