from SlideReader import SlideReader
from Patch import Patch
from PIL import Image
import numpy as np
from skimage.color import rgb2gray
import skimage
from skimage import data, io
from skimage.viewer import ImageViewer
from skimage.measure import label, regionprops

##Constants##
initial_slide_level = 6
mask_level_zero = 0
slide_level_zero = 0
sample_patch_size = 3000
valid_bw_value = 0.75
sample_count = 1000
initial_mask_level = 6

total_mask=0
total_region=0
total_out_size=0

for rr in range(1,110):
    maskpath = '/run/media/oguzhaner/Seagate Expansion Drive/Thesis/camelyon16/TrainingData/Ground_Truth/Mask/Tumor_'
    slidepath = '/run/media/oguzhaner/Seagate Expansion Drive/Thesis/camelyon16/TrainingData/Train_Tumor/Tumor_'

    if rr < 10:
        maskpath = maskpath + "00" + str(rr) + "_Mask.tif"
        slidepath = slidepath + "00" + str(rr) + ".tif"
    if (rr >= 10) and (rr <100):
        maskpath = maskpath + "0" + str(rr) + "_Mask.tif"
        slidepath = slidepath + "0" + str(rr) + ".tif"
    if  (rr >= 100):
       maskpath = maskpath + str(rr) + "_Mask.tif"
       slidepath = slidepath + str(rr) + ".tif"

    reader = SlideReader(slidepath, maskpath)
    dims = reader.get_level_mask_dims(initial_mask_level)

    patch = Patch(location=[0, 0], size=[dims[0], dims[1]])
    I = reader.get_mask_content(patch,initial_mask_level)
    Ilbl = skimage.morphology.label(I)

    regions = regionprops(Ilbl.astype(int))
    counter = 0
    # print rr
    print maskpath
    print slidepath
    for prop in regions:
        # print "region" + str(counter)
        counter=counter+1
        y0,x0 = prop.centroid

        level_zero_x = x0*64
        level_zero_y = y0*64

        xmin = level_zero_x -(sample_patch_size/2)
        ymin = level_zero_y - (sample_patch_size/2)

        patch=Patch(location=[int(xmin),int(ymin)],size=[sample_patch_size,sample_patch_size])
        slideContent=reader.read_area_content(slide_level_zero,patch)
        maskContent=reader.get_mask_content(patch,mask_level_zero)

        slideImage=Image.fromarray(slideContent)
        slideImage.save("/run/media/oguzhaner/Seagate Expansion Drive/Thesis/data_set/tumor/slide_tumor/slide_" + str(rr) +"_region_" + str(counter) + ".jpg")
        maskImage=Image.fromarray(maskContent)
        maskImage.convert('RGB').save("/run/media/oguzhaner/Seagate Expansion Drive/Thesis/data_set/tumor/mask/mask_" + str(rr) + "_region_" + str(counter) + ".jpg")
