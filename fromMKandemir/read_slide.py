import skimage
from SlideReader import SlideReader
from Patch import Patch
import skimage.color
import skimage.morphology
import skimage.io as skio
import matplotlib.pyplot as plt
import numpy as np
#import os    
#os.environ['THEANO_FLAGS'] = "device=cpu"  
import theano
import pickle
import vgg_s_prnet
import lasagne
import prep_image
import rectum_net
from theano import tensor as T
import cPickle
# Prepare the pretrained net
#model=pickle.load( open("/home/mkandemi/Documents/pretrained_nets/vgg_cnn_s.pkl","r"))
#CLASSES = model['synset words']
#MEAN_IMAGE = model['mean image']
#net=vgg_s_prnet.build_model()
#lasagne.layers.set_all_param_values(net, model['values'][0:14])
input_var= T.ftensor4('inputs')
net_features = rectum_net.build_features(input_var)
input_var2= T.ftensor4('inputs')
net_classes = rectum_net.build_classes(input_var2,9)
params = np.load('/home/mkandemi/Documents/pretrained_nets/rectum_network_params.npz')['params']
lasagne.layers.set_all_param_values(net_features, params[0:14])
lasagne.layers.set_all_param_values(net_classes, params[14:16])

for ii in range(0,150):
        
    if ii+1 > 99:
        imstr = str(ii+1)
    elif ii+1 > 9:
        imstr = '0' + str(ii+1)
    else:
        imstr = '00' + str(ii+1)        
        
    slidepath='/home/mkandemi/Documents/data/CAMELYON16/TrainingData/Train_Tumor/Tumor_' +imstr+ '.tif'
    annotpath='/home/mkandemi/Documents/data/CAMELYON16/TrainingData/Ground_Truth/XML/Tumor_' +imstr+ '.xml'
    maskpath= '/home/mkandemi/Documents/data/CAMELYON16/TrainingData/Ground_Truth/Mask/Tumor_' +imstr+ '_Mask.tif'
    
    reader = SlideReader(slidepath, maskpath)
    
    # Find the foreground
    dims=reader.get_level_dims(4)
    patch = Patch(location=[0,0],size=[dims[0],dims[1]])
    I=reader.read_area_content(4,patch)
    Ig=skimage.color.rgb2gray(I)
    Ibw = (Ig > 0.04) * (Ig < 0.7)
    Ibw = skimage.morphology.remove_small_holes(Ibw,20000)
    Ibw = skimage.morphology.remove_small_objects(Ibw,20000)
    Ibw[0:2000,:] = False;  #Ibw[:,0:200] = False
    Ibw[12000:14000,:] = False;  #Ibw[:,2900:3000] = False
    Ilbl = skimage.morphology.label(Ibw)
    num_regions = np.max(Ilbl)   
    patch_size = 256
    
    fns = 0
    tps = 0
    fps = 0
    tns = 0
    
    # Read the mask
    sz=reader.mask.level_dimensions[4]
    Imask = np.array(reader.mask.read_region((0,0), 4, (sz[0],sz[1])))[:,:,0]*1.0
    #imshow(Imask)
    for rr in range(num_regions): # Loop over foreground components
        print rr
        cnt=0
        Irr = Ilbl==(rr+1)
        active_area=np.where(Irr)
        xmin=np.max(np.min(active_area[1])-15,0)*16; xmax=np.max(active_area[1])*16
        ymin=np.max(np.min(active_area[0])-15,0)*16; ymax=np.max(active_area[0])*16
        
        X=np.zeros([10000,521])
        y=np.zeros([10000,])
                
        for xx in np.arange(xmin,xmax,patch_size/2):
            for yy in np.arange(ymin,ymax,patch_size/2):      
               patch = Patch(location=np.asarray([xx,yy]),size=np.asarray([patch_size,patch_size] ))
               Ipatch = reader.read_area_content(0,patch)[:,:,0:3]
               Ig_patch=skimage.color.rgb2gray(Ipatch)
               Ibw_patch = (Ig_patch > 0.04) * (Ig_patch < 0.7)
               if np.mean(Ibw_patch) > 0.75:
                   label = reader.get_area_label(patch)
                   Ipatch = prep_image.prep_image(Ipatch)
                   feature_vector = lasagne.layers.get_output(net_features, Ipatch, deterministic=True).eval()                                                         
                   classes = lasagne.layers.get_output(net_classes, feature_vector, deterministic=True).eval()
                   classes =np.array(classes.flatten());  feature_vector = np.array(feature_vector.flatten())
                   #ranking=np.argsort(-classes)+1
                   
                   #is_proposal = sum(np.in1d(ranking[0:3],[1,3,9]))>0
                   
                   #if is_proposal==True and label==True:
                   #    tps += 1                       
                   #if is_proposal==False and label==True:                       
                   #    fns += 1
                   #if is_proposal==True and label==False:                       
                   #    fps += 1
                   #if is_proposal==False and label==False:                       
                   #    tns += 1
                   
                   X[cnt,:] = np.concatenate((feature_vector,classes))
                   y[cnt] = label
                   cnt+=1                                
                   
                  # if np.mod(cnt,25) == 0:
                  #     print rr, "tp:", tps, "tn:", tns, "fp:", fps, "fn:", fns
        #print "End of", rr, "tp:", tps, "tn:", tns, "fp:", fps, "fn:", fns
        X=X[0:cnt,:]; y[0:cnt]
        data = {'X': X, 'y': y}                   
        with open('/home/mkandemi/Documents/data/CAMELYON16/bags/slide' + str(ii) + 'comp' + str(rr) + '.pkl' , 'wb') as f:
            cPickle.dump(data, f)
            
                   #if np.argmax(classes) == 0:
                   #    xxx=0;
                   #label = reader.get_area_label(patch)
#        
    #skio.imsave('/home/mkandemi/Desktop/temp/' + imstr + '.jpg',I)


