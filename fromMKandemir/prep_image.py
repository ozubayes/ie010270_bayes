import skimage.transform
import numpy as np
from lasagne.utils import floatX

#def prep_image(im,MEAN_IMAGE):
def prep_image(im):
    #ext = url.split('.')[-1]
    #im = plt.imread(io.BytesIO(urllib.urlopen(url).read()), ext)
    # Resize so smallest dim = 256, preserving aspect ratio
    h, w, _ = im.shape
    if h < w:
        im = skimage.transform.resize(im, (256, w*256/h))
    else:
        im = skimage.transform.resize(im, (h*256/w, 256))

#    # Central crop to 224x224
#    h, w, _ = im.shape
#    im = im[h//2-112:h//2+112, w//2-112:w//2+112]
        
    # Shuffle axes to c01
    im = np.swapaxes(np.swapaxes(im, 1, 2), 0, 1)
    
    # Convert to BGR
    im = im[::-1, :, :]

#    im = im - MEAN_IMAGE
    return floatX(im[np.newaxis])